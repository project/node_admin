<?php

/**
 * @file
 * Contains page callbacks of Node Admin module.
 */

/**
 * Page callback.
 *
 * @param string $type
 *   The node type.
 *
 * @return mixed
 *   The rendered output
 */
function node_admin_page($type) {
  $node_type_select_form = drupal_get_form('node_admin_type_select_form');
  return drupal_render($node_type_select_form) . views_embed_view('node_admin', 'block_1', $type);
}
